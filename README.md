# NBP SpringBoot REST Client (ver. Demo)

Aplikacja oparta na SpringBoot wykorzystująca REST API NBP do pobrania i przetwarzania danych z cenami złota.

## Użycie

Aby skorzystać z aplikacji wystarczy pobrać z repozytorium plik hertz-0.0.1-SNAPSHOT.jar a następnie odpalić za pomocą komendy

```bash
java -jar hertz-0.0.1-SNAPSHOT.jar
```
Po odpaleniu pliku jar uruchomi się localhost:8080  
Aby uzyskać potrzebne dane, należy w oknie przeglądarki wpisać odpowiedni adres:
```java
//Front-end aplikacji
localhost:8080 

//Podejrzenie bazy danych
localhost:8080/h2 

//Lista cen złota za ostatnich 30 dni
localhost:8080/list 

//Cena złota z dnia dzisiejszego
localhost:8080/today

//Średnia cena złota z ostatnich 30 dni
localhost:8080/avg

//Minimalna cena złota z ostatnich 30 dni
localhost:8080/min

//Maksymalna cena złota z ostatnich 30 dni
localhost:8080/max
```
Wszystkie dane wyświetlane są w formacie JSON