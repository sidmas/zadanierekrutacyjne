package com.zadanie.hertz;
import com.zadanie.hertz.mapper.JsonMapper;
import com.zadanie.hertz.services.SessionCreator;
import com.zadanie.hertz.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.properties")
public class HertzApplication implements CommandLineRunner {
    @Value("${nbp.url}")
    private String url;

	public static void main(String[] args) {
		SpringApplication.run(HertzApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

			SessionCreator sessionCreator = new SessionCreator();
			sessionCreator.createSession(new JsonMapper(url));


	}
}
