package com.zadanie.hertz.converter;

import com.google.gson.Gson;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.Map;

public class QueryToJsonConverter {
    public static String convertResultToJson(final String key, final Query query){
        Map<String,Object> goldMap = new HashMap<>();
        goldMap.put(key,query.getSingleResult().toString());
        Gson gson = new Gson();
        return gson.toJson(goldMap);

    }
}
