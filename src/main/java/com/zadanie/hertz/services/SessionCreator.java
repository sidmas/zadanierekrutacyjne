package com.zadanie.hertz.services;

import com.zadanie.hertz.mapper.JsonMapper;
import com.zadanie.hertz.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;

public class SessionCreator {
    public void createSession(JsonMapper jsonMapper) throws IOException {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        tx = session.beginTransaction();
        for(int i = 0 ; i < jsonMapper.mapJsonToGoldList().size(); i++){
            session.save(jsonMapper.mapJsonToGoldList().get(i));
        }
        tx.commit();
        session.close();
    }
}
