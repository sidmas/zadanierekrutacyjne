package com.zadanie.hertz.entities;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@SequenceGenerator(name="seq",initialValue = 1)
@Table(name="GOLD")
public class GoldEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seq")
    @JsonIgnore
    @Column(name="ID")
    private Integer id;

    @Column(name="DATE")
    private String data;

    @Column(name="PRICE")
    private BigDecimal cena;
    public GoldEntity(){}

    public void setId(Integer id) {
        this.id = id;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public Integer getId() {
        return id;
    }

    public String getDate() {
        return data;
    }

    public BigDecimal getPrice() {
        return cena;
    }
}
