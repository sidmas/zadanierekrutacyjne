package com.zadanie.hertz.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zadanie.hertz.services.RestClient;
import com.zadanie.hertz.entities.GoldEntity;

import java.io.IOException;
import java.util.*;

public class JsonMapper {
    private String url;
    private String jsonResponse;
    public JsonMapper(String url){this.url = url;}
    private void setJsonResponse() {
        this.jsonResponse = RestClient.callGet(url);
    }
    private String getJsonResponse() {
        return jsonResponse;
    }
    public List<GoldEntity> mapJsonToGoldList() throws IOException {
        setJsonResponse();
        ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(getJsonResponse(), GoldEntity[].class));
    }
}
