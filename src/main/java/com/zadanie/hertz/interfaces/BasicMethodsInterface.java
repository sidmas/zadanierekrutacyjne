package com.zadanie.hertz.interfaces;

import com.zadanie.hertz.entities.GoldEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface BasicMethodsInterface extends JpaRepository<GoldEntity,Integer> {
}
