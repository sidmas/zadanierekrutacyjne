package com.zadanie.hertz.utils;
import com.zadanie.hertz.entities.GoldEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
public class HibernateUtils {
    private static final SessionFactory sessionFactory;
    static {
        try {
            Configuration config = new Configuration();
            config.addAnnotatedClass(GoldEntity.class);
            sessionFactory = config.configure().buildSessionFactory();
        } catch (Throwable e) {
            System.err.println("Error in creating SessionFactory object."
                    + e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
