package com.zadanie.hertz.controller;
import com.zadanie.hertz.entities.GoldEntity;
import com.zadanie.hertz.interfaces.GoldRepository;
import com.zadanie.hertz.converter.QueryToJsonConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@RestController
public class EntityController {
    private static Query query;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private
    GoldRepository goldRepository;
    @GetMapping("/list")
    public List<GoldEntity> index(){
        return goldRepository.findAll();
    }
    @GetMapping("/avg")
    public String avgGoldPrice(){
        query =  entityManager.createQuery(Constants.AVG);
        return QueryToJsonConverter.convertResultToJson("price",query);
    }
    @GetMapping("/max")
    public String maxGoldPrice(){
        query =  entityManager.createQuery(Constants.MAX);
        return QueryToJsonConverter.convertResultToJson("price",query);
    }
    @GetMapping("/min")
    public String minGoldPrice(){
        query =  entityManager.createQuery(Constants.MIN);
        return QueryToJsonConverter.convertResultToJson("price",query);
    }
    @GetMapping("/today")
    public String todayGoldPrice(){
        query =  entityManager.createQuery(Constants.ACTUAL);
        return QueryToJsonConverter.convertResultToJson("price",query);
    }
}
