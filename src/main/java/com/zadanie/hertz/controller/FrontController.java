package com.zadanie.hertz.controller;
import com.zadanie.hertz.interfaces.GoldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Controller
public class FrontController {
    private static Map<String,Query> queryMap;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private
    GoldRepository goldRepository;
    @RequestMapping("/")
    public String index(ModelMap modelMap){

        createQueryMap();
        modelMap.addAttribute("goldlist",goldRepository.findAll());
        modelMap.addAttribute("avg", Float.parseFloat(queryMap.get("avg").getSingleResult().toString()));
        modelMap.addAttribute("max",queryMap.get("max").getSingleResult());
        modelMap.addAttribute("min",queryMap.get("min").getSingleResult());
        modelMap.addAttribute("actual",queryMap.get("actual").getSingleResult());
        return "index";
    }
    private void createQueryMap(){
        queryMap = new HashMap<>();
        queryMap.put("avg",entityManager.createQuery(Constants.AVG));
        queryMap.put("min",entityManager.createQuery(Constants.MIN));
        queryMap.put("max",entityManager.createQuery(Constants.MAX));
        queryMap.put("actual",entityManager.createQuery(Constants.ACTUAL));
    }
}
