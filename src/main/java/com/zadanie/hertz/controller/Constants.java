package com.zadanie.hertz.controller;

class Constants {
    static final String AVG = "select avg(e.cena) from GoldEntity e";
    static final String MAX = "select max(e.cena) from GoldEntity e";
    static final String MIN = "select min(e.cena) from GoldEntity e";
    static final String ACTUAL = "select e.cena from GoldEntity e where e.data = CURDATE()";
}
